import { Component } from "react";
import Display from "./Display";

class Parent extends Component{
    constructor(props) {
        super(props)

        this.state = {
            chilDisplay: true
            }
        }

    updateDisplay = () => {
        this.setState({
            chilDisplay: false
        })
    }

    render(){
        return(
            <>
            {this.state.chilDisplay ? <Display buttonShow={this.updateDisplay}/> : null}
            </>
        )
    }
}

export default Parent;