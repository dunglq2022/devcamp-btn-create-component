import { Component } from "react";

class Display extends Component {
    constructor(props) {
        super(props);

        this.state = ({
            buttonShow: true,
            style: 'btn btn-info text-white border border-dark',
            text: "Create Component",
            textH1: ""
        })
    }   
    
    changedButtonDestroy = () => {
        this.setState({
            buttonShow: false,
            style: 'btn btn-danger text-dark',
            text: "Destroy Component",
            textH1: "exit!"
        })
        console.log("Change Button Danger")
    }

    changedRemoveButton = () => {
        this.setState({
            buttonShow: true,
        })
        console.log("Remove Button")
        console.log(this.state.buttonShow)
    }

    componentDidUpdate () {
        let removeButton = this.state.buttonShow
        console.log('Component Did Update');
        if (removeButton === true) {
            this.props.buttonShow();
        }
    }
      
    render() {
        return (
            <div className="container mt-4">
                <button onClick={this.state.buttonShow ? this.changedButtonDestroy : this.changedRemoveButton} className={this.state.style}>
                    {this.state.text}
                </button>
                <h1>{this.state.textH1}</h1>
            </div>
        )
    }
}

export default Display;